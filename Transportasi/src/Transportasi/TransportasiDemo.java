package Transportasi;

public class TransportasiDemo {
    public static void main(String[] args) {
        // Create objects of the classes
        Mobil mobil1 = new Mobil("Toyota", 2015, "Avanza Type G", "Bensin", "Hitam");
        Sepeda sepeda1 = new Sepeda("Polygon", 2022, "Premier", "Leisure", "Grey");

        // Call the methods of the classes
        System.out.println("Informasi Mobil:");
        System.out.println("----------------");
        mobil1.tampilInfo();
        System.out.println();

        System.out.println("Informasi Sepeda:");
        System.out.println("-----------------");
        sepeda1.tampilInfo();
        System.out.println();
    }
}